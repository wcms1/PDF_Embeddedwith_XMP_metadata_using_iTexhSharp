﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Runtime.InteropServices
Imports System.Security.Principal
Imports System.Data.SqlClient
Imports System.Text
Imports iTextSharp.text
Imports iTextSharp.text.xml.xmp
Imports iTextSharp.text.pdf
Imports System.Deployment.Application
Imports System.Net.Mail

Public Class Form1
    Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal un As String, ByVal domain As String, ByVal pw As String, ByVal LogonType As Integer, ByVal LogonProvider As Integer, ByRef Token As IntPtr) As Boolean
    Public Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Boolean
    Public DbConnectionString As String = "Data Source=" & TechsetSqlIp() & ";Initial Catalog=JTS;User Id=TIAT;Password=jtstest123;"


    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click
        Me.Timer1.Enabled = True
        Me.Timer1.Start()
        Me.btnStart.Enabled = False
        Me.btnStop.Enabled = True
        Me.txtIn.Enabled = False
        Me.txtOut.Enabled = False
    End Sub

    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click
        Me.Timer1.Stop()
        Me.Timer1.Enabled = False
        Me.btnStart.Enabled = True
        Me.btnStop.Enabled = False
        Me.txtIn.Enabled = True
        Me.txtOut.Enabled = True
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        'Try

        'Dim InPath = Me.txtIn.Text.Trim
        'Dim OutPath = Me.txtOut.Text.Trim

        CheckRestart()

        Dim InPath = "Y:"
        Dim OutPath = "Z:"

        Dim di As New DirectoryInfo(InPath)
        Dim aryFi As FileInfo() = di.GetFiles("*.pdf*", SearchOption.TopDirectoryOnly)
        'Dim singleFile As FileInfo
        'MsgBox(aryFi.Length)

        If aryFi.Length > 0 Then
            For Each singleFile As FileInfo In aryFi
                If InStr(singleFile.Name, "smart-track", CompareMethod.Text) > 0 Then
                    Try
                        File.Delete(singleFile.FullName)
                    Catch ex As Exception

                    End Try
                    GoTo DoNext
                End If

                Me.Timer1.Stop()
                Me.Timer1.Enabled = False
                'MsgBox(singleFile.FullName)

                Dim TxtFile As String = Replace(singleFile.FullName, ".pdf", ".txt")
                If File.Exists(TxtFile) Then
                    WriteLog("Smart Track" & vbCrLf & Format(Now, "HH:mm:ss") & vbTab & singleFile.Name & vbCrLf)
                    Shell("D:\SmartTrack\AutoBatch_Smart-track.bat " & """" & singleFile.FullName & """", AppWinStyle.NormalFocus, True)
                    If File.Exists(Replace(singleFile.FullName, ".pdf", "_smart-track.pdf")) Then
                        WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Smart track pdf generated" & vbCrLf)

                        Dim TxtCont As String = ReadFile(TxtFile)
                        OutPath = Regex.Match(TxtCont, "[^\n\r]+", RegexOptions.IgnoreCase).Value
                        Me.txtOut.Text = OutPath
                        Try
                            File.Delete(TxtFile)
                        Catch ex As Exception
                            WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Could not delete txt file (" & TxtFile & ")." & vbCrLf)
                        End Try
                        If File.Exists(OutPath & "\" & singleFile.Name) Then
                            WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Trying to delete the output pdf which already exist" & vbCrLf)
                            Try
                                File.Delete(OutPath & "\" & singleFile.Name)
                                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Deleted existing pdf (" & OutPath & "\" & singleFile.Name & ")." & vbCrLf)
                            Catch ex As Exception
                                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Could not delete " & singleFile.FullName & "(" & ex.Message & "). Old file shall be uploaded." & vbCrLf)
                                'Shell("\\techsetserver1\software\tandf\smarttrack\sendmail.exe " & """" & "Could not delete " & singleFile.FullName & "(" & ex.Message & "). Old file shall be uploaded." & """", AppWinStyle.Hide)
                                GoTo DoNext
                            End Try

                        End If

                        Dim Jacr As String = Regex.Match(singleFile.Name, "^[A-Z]{4}", RegexOptions.IgnoreCase).Value.ToString
                        Dim ArtId As String = Regex.Match(singleFile.Name, "\d+", RegexOptions.IgnoreCase).Value.ToString
                        Dim ArtName As String = ""
                        If Jacr <> "" And ArtId <> "" Then ArtName = Jacr & ArtId


                        Try
                            File.Copy(Replace(singleFile.FullName, ".pdf", "_smart-track.pdf"), OutPath & "\" & singleFile.Name, True)
                            WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Successfully created " & OutPath & "\" & singleFile.Name & vbCrLf)
                        Catch ex As Exception
                            WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Could not move smart track pdf into output path (" & OutPath & "). (" & ex.Message & ")" & vbCrLf)
                            'Shell("\\techsetserver1\software\tandf\smarttrack\sendmail.exe " & """" & "Could not move smart track pdf into output path (" & ex.Message & ")" & singleFile.FullName & """", AppWinStyle.Hide)
                            Dim AllAttachments As New List(Of String)
                            If MailViaRelay("automailer@novatechset.com", "vijayakumar@novatechset.com", "", "", "SmartTrack File Movement Problem: " & singleFile.Name, "Hi," & vbCrLf & vbCrLf & OutPath & "\" & singleFile.Name, AllAttachments) = True Then
                            Else
                                MsgBox("Unable to send Mail!", MsgBoxStyle.Critical)
                            End If

                            GoTo SkipDbUpdate
                        End Try
                        Try
                            File.Delete(Replace(singleFile.FullName, ".pdf", "_smart-track.pdf"))
                        Catch ex As Exception

                        End Try


                        If ArtName <> "" Then
                            UpdateSql("UPDATE Article SET Artcl_TnFPDFDT=GETDATE() WHERE Artcl_ArticleId='" & ArtName & "'")
                        Else
                            MsgBox("could not get the article name", MsgBoxStyle.Critical)
                        End If

SkipDbUpdate:

                        Try
                            File.Delete(singleFile.FullName)
                        Catch ex As Exception

                        End Try

                    Else
                        WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Smart track pdf not generated" & vbCrLf)
                        'Shell("\\techsetserver1\software\tandf\smarttrack\sendmail.exe " & """" & "Smart track pdf not generated for " & singleFile.FullName & """", AppWinStyle.Hide)
                    End If
                Else
                    WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Text file (" & TxtFile & ") not found!" & vbCrLf)
                End If

DoNext:
                'Exit For
            Next
        Else
            WriteLog("No file to process")
        End If


        Dim InPath1 = "W:"
        Dim Outpath1 = "X:"
        Dim ErrorPath1 = InPath1 & "\" & "Error"
        If Not Directory.Exists(ErrorPath1) Then Directory.CreateDirectory(ErrorPath1)
        Dim di1 As New DirectoryInfo(InPath1)
        Dim aryFi1 As FileInfo() = di1.GetFiles("*.pdf*", SearchOption.TopDirectoryOnly)
        Dim singleFile1 As FileInfo
        'MsgBox(aryFi1.Length)
        If aryFi1.Length > 0 Then

            For Each singleFile1 In aryFi1
                If InStr(singleFile1.Name, "_initial-view", CompareMethod.Text) > 0 Or InStr(singleFile1.Name, "_xmp", CompareMethod.Text) > 0 Then
                    Try
                        File.Delete(singleFile1.FullName)
                    Catch ex As Exception

                    End Try
                    GoTo DoNext1
                End If


                Dim TxtFile As String = Replace(singleFile1.FullName, ".pdf", ".txt")
                If Not File.Exists(TxtFile) Then
                    WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Text file (" & TxtFile & ") not found!" & vbCrLf)
                    'Try
                    '    File.Copy(singleFile1.FullName, ErrorPath1 & "\" & singleFile1.Name.Replace(".pdf", "_dontuse.pdf"), True)
                    '    File.Delete(singleFile1.FullName)
                    '    WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Moved " & singleFile1.Name.Replace(".pdf", "_dontuse.pdf") & " into " & ErrorPath1 & vbCrLf)
                    'Catch ex As Exception
                    '    WriteLog(Format(Now, "HH: mm:ss") & vbTab & "Could not move " & singleFile1.Name.Replace(".pdf", "_dontuse.pdf") & " into " & ErrorPath1 & vbCrLf)
                    'End Try
                    GoTo DoNext1
                End If

                WriteLog("Initial View" & vbCrLf & Format(Now, "HH:mm:ss") & vbTab & singleFile1.Name & vbCrLf)

                Dim Jacr As String = Regex.Match(singleFile1.Name, "^[A-Z]{4}", RegexOptions.IgnoreCase).Value.ToString
                Dim ArtId As String = Regex.Match(singleFile1.Name, "\d+", RegexOptions.IgnoreCase).Value.ToString
                Dim ArtName As String = ""
                If Jacr <> "" And ArtId <> "" Then ArtName = Jacr & ArtId


                Me.Timer1.Stop()
                Me.Timer1.Enabled = False
                Shell("D:\SmartTrAutoBatch_Initialack\-view.bat " & """" & singleFile1.FullName & """", AppWinStyle.NormalFocus, True)

                Dim Initial_view_pdf As String = Replace(singleFile1.FullName, ".pdf", "_initial-view.pdf")

                If File.Exists(Initial_view_pdf) Then
                    WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Initial View pdf generated" & vbCrLf)

                    'Dim TxtFile As String = Replace(singleFile1.FullName, ".pdf", ".txt")

                    Dim TxtCont As String = ReadFile(TxtFile)
                    Outpath1 = Regex.Match(TxtCont, "[^\n\r]+", RegexOptions.IgnoreCase).Value

                    Dim IsIssue As Boolean = Regex.Match(TxtCont, "\\ISSUE\\", RegexOptions.IgnoreCase).Success

                    Me.txtOutIv.Text = Outpath1
                        Try
                            File.Delete(TxtFile)
                        Catch ex As Exception
                            WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Could not delete txt file (" & TxtFile & ")." & vbCrLf)
                        End Try

                        If File.Exists(Outpath1) Then
                            WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Trying to delete the output pdf which already exist" & vbCrLf)
                            Try
                                File.Delete(Outpath1)
                                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Deleted" & vbCrLf)
                            Catch ex As Exception
                                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Could not delete " & singleFile1.FullName & "(" & ex.Message & "). Old file shall be uploaded." & vbCrLf)
                            'Shell("\\techsetserver1\software\tandf\smarttrack\sendmail.exe " & """" & "Could not delete " & singleFile1.FullName & "(" & ex.Message & "). Old file shall be uploaded." & """", AppWinStyle.Hide)
                            If Not IsIssue Then preivewfilecheck(ArtName, False)
                            GoTo DoNext1
                            End Try

                        End If

                        Dim xmpPath As String = Replace(Outpath1, ".pdf", ".xmp")
                        If File.Exists(xmpPath) Then
                            If AppendXMP(Initial_view_pdf, xmpPath) = False Then
                                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "XMP insertion failed" & vbCrLf)
                            'If Not IsIssue Then preivewfilecheck(ArtName, False)
                            'GoTo DoNext1
                        End If
                        Else
                        WriteLog(Format(Now, "HH:mm:ss") & vbTab & "File nfot found: " & xmpPath & vbCrLf)
                    End If

                        Try
                            File.Copy(Initial_view_pdf, Outpath1, True)
                            Try
                                File.Delete(Initial_view_pdf)
                            Catch ex As Exception

                            End Try

                            WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Successfully created " & Outpath1 & "." & vbCrLf)
                        Catch ex As Exception
                            WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Could not move Initial View pdf into output path [" & Outpath1 & "]. (" & ex.Message & ")" & vbCrLf)
                            Try
                                File.Copy(Initial_view_pdf, ErrorPath1 & "\" & singleFile1.Name, True)
                                File.Delete(Initial_view_pdf)
                                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Moved " & singleFile1.Name & " into " & ErrorPath1 & vbCrLf)
                            Catch ex1 As Exception
                                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Could not move " & singleFile1.Name & " into " & ErrorPath1 & vbCrLf)
                            End Try
                        If Not IsIssue Then preivewfilecheck(ArtName, False)
                        GoTo DoNext1
                            'Shell("\\techsetserver1\software\tandf\smarttrack\sendmail.exe " & """" & "Could not move Initial View pdf (" & singleFile1.FullName & ") into output path (" & ex.Message & ")" & Outpath1 & """", AppWinStyle.Hide)
                        End Try

                        Try
                            File.Delete(singleFile1.FullName)
                        Catch ex As Exception

                        End Try
                    If Not IsIssue Then preivewfilecheck(ArtName, True)
                Else
                        WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Initial View pdf not generated" & vbCrLf)

                    'Shell("\\techsetserver1\software\tandf\smarttrack\sendmail.exe " & """" & "Initial View pdf not generated for " & singleFile1.FullName & """", AppWinStyle.Hide)
                End If
DoNext1:
                'Exit For
            Next
        Else
            WriteLog("No file to process")
        End If

        Me.Timer1.Enabled = True
        Me.Timer1.Start()

        'Catch ex As Exception
        ' MsgBox(ex.Message)
        'End Try




    End Sub

    Private Sub Form1_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Dim objWriter As StreamWriter
        Try
            objWriter = New StreamWriter("D:\SmartTrack/" & "settings.ini", False, System.Text.Encoding.UTF8)
            objWriter.WriteLine(Me.txtIn.Text)
            objWriter.WriteLine(Me.txtOut.Text)
            objWriter.Close()
        Catch Ex As Exception
            MsgBox(Ex.Message)
        End Try

        objWriter = Nothing

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'AppendXMP("D:\Vijay\XMP\PLAT1862141.pdf", "D:\Vijay\XMP\PLAT1862141.xmp")
        'Dim fileCont = ReadFile("D:\SmartTrack/" & "settings.ini")
        'Try
        '    Me.txtIn.Text = Split(fileCont, Chr(13))(0)
        '    Me.txtOut.Text = Split(fileCont, Chr(13))(1)
        'Catch ex As Exception
        '    'MsgBox("Acrobat path not defined.", MsgBoxStyle.Critical)
        'End Try
        'AppendXMP("D:\Vijay\Projects\xmp_tandf\CAIC1566511.pdf", "D:\Vijay\Projects\xmp_tandf\CAIC1566511.xmp")

        Dim myVersion As System.Version

        If ApplicationDeployment.IsNetworkDeployed Then
            myVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion
            Me.Text &= " - " & myVersion.ToString
            MapDrives()

        End If

        Me.txtIn.Text = "Y:"
        Me.txtOut.Text = "Z:"
        Me.txtInIv.Text = "W:"
        Me.txtOutIv.Text = "X:"
        delete_unwanted_files()
        btnStart_Click(sender, EventArgs.Empty)
    End Sub
    Private Sub delete_unwanted_files()
        Dim di As New DirectoryInfo("W:\")
        Dim aryFi As FileInfo() = di.GetFiles("*.bak", SearchOption.TopDirectoryOnly)
        Dim singleFile As FileInfo

        If aryFi.Length > 0 Then
            For Each singleFile In aryFi
                Try
                    File.Delete(singleFile.FullName)
                Catch ex As Exception

                End Try
            Next
        End If
        di = New DirectoryInfo("Y:\")
        aryFi = di.GetFiles("*.bak", SearchOption.TopDirectoryOnly)

        'MsgBox(aryFi1.Length)
        If aryFi.Length > 0 Then
            For Each singleFile In aryFi
                Try
                    File.Delete(singleFile.FullName)
                Catch ex As Exception

                End Try
            Next
        End If

    End Sub
    Private Sub MapDrives()
        If Not Directory.Exists("Y:\") Or Not Directory.Exists("W:\") Then
            Shell("net use * /DELETE /yes", AppWinStyle.NormalFocus, True)
            Shell("net use Y: \\techsetserver2\upload\TandFSmartTrack\IN", AppWinStyle.NormalFocus, True)
            Shell("net use W: \\techsetserver2\upload\TandFSmartTrack\InitialView", AppWinStyle.NormalFocus, True)
        End If
    End Sub
    Public Function ReadFile(ByVal FullPath As String) As String
        Dim objReader As StreamReader
        Dim bAns As String = ""
        Try
            objReader = New StreamReader(FullPath, System.Text.Encoding.Default, True)
            bAns = objReader.ReadToEnd()
            objReader.Close()
        Catch Ex As Exception
        End Try
        Return bAns
    End Function
    Private Sub CheckRestart()
        Dim todayFile As String = "D:\Restarted_" & Format(Today, "yyyy-MM-dd") & ".txt"
        Dim ydayFile As String = "D:\Restarted_" & Format(Today.AddDays(-1), "yyyy-MM-dd") & ".txt"

        If File.Exists(ydayFile) Then File.Delete(ydayFile)

        If Not File.Exists(todayFile) And Convert.ToInt32(Format(Now, "HH")) > 9 Then
            Dim ObjWriter As StreamWriter

            Try
                ObjWriter = New StreamWriter(todayFile, True, System.Text.Encoding.UTF8)
                ObjWriter.Write("done")
                ObjWriter.Close()
            Catch Ex As Exception
                MsgBox(Ex.Message)
            End Try

            WriteLog("Tool stopped for scheduled restart")
            Shell("shutdown /r /t 0")
        End If
    End Sub
    Public Sub WriteLog(ByVal Text As String)


        If Not Directory.Exists("D:\SmartTrack\Log\") Then Directory.CreateDirectory("D:\SmartTrack\Log\")
        Dim LogFile = "D:\SmartTrack\Log\" & Format(Now, "ddMMyyyy") & ".htm"

        Dim ObjWriter As StreamWriter

        Try
            ObjWriter = New StreamWriter(LogFile, True, System.Text.Encoding.UTF8)
            ObjWriter.Write("<tr><td>" & Now & vbTab & Text & "</td></tr><br>")
            ObjWriter.Close()
        Catch Ex As Exception
            MsgBox(Ex.Message)
        End Try

        If Not Regex.Match(Text, "(No file to process|Text File )", RegexOptions.IgnoreCase).Success Then
            Me.RichTextBox1.AppendText(Text)
            Try

                ObjWriter = New StreamWriter("\\techsetserver1\software\DataProcessing\Macros\Bin\Log\smart_track\" & Format(Now, "dd-MM-yyyy") & ".txt", True)
                ObjWriter.Write(Now & vbTab & Text)
                ObjWriter.Close()
            Catch ex As Exception
                'MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub RichTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RichTextBox1.TextChanged
        Me.RichTextBox1.SelectionStart = RichTextBox1.TextLength
        Me.RichTextBox1.ScrollToCaret()
    End Sub
    Function MoveFile(ByVal Src As String, ByVal Dest As String) As Boolean
        Dim tokenHandle As New IntPtr(0)
        Try
            If LogonUser("upload", "TECHSETINDIA", "techset12", 2, 0, tokenHandle) Then
                Dim newId As New WindowsIdentity(tokenHandle)
                Using impersonatedUser As WindowsImpersonationContext = newId.Impersonate()
                    'perform impersonated commands
                    Try
                        File.Copy(Src, Dest, True)
                        File.Delete(Src)
                        WriteLog("Smart Track" & vbCrLf & Format(Now, "HH:mm:ss") & vbTab & Src & " copied into " & Dest & vbCrLf)
                        Return True
                    Catch ex As Exception
                        WriteLog("Smart Track" & vbCrLf & Format(Now, "HH:mm:ss") & vbTab & Src & " could not copy into " & Dest & vbCrLf & ex.Message & vbCrLf)
                    End Try

                End Using
                CloseHandle(tokenHandle)
            Else
                'logon failed
                'MsgBox("Logon failed")
            End If
        Catch ex As Exception
            'exception
            MsgBox(ex.Message)
        End Try
        Return False
    End Function
    Public Function TechsetSqlIp()
        TechsetSqlIp = ReadFile("\\techsetserver1\software\General\TechsetSqlIp.txt").Trim
    End Function
    Public Function QuerySql(ByVal MyQuery As String)

        Dim Cmd As SqlCommand
        Dim dr As SqlDataReader

        Dim Cn As New SqlConnection(DbConnectionString)
        Cn.Open()
        Cmd = New SqlCommand(MyQuery, Cn)
        dr = Cmd.ExecuteReader
        While dr.Read
            Return Trim(dr(0))
        End While
        Cn.Close()
    End Function
    Public Function UpdateSql(ByVal MyQuery As String) As Integer

        WriteLog(Format(Now, "HH:mm:ss") & vbTab & "SQL Query: " & MyQuery & vbCrLf)
        Dim Cmd As SqlCommand

        Dim Cn As New SqlConnection(DbConnectionString)
        Cn.Open()
        Cmd = New SqlCommand(MyQuery, Cn)
        Dim AffectedRows As Integer = Cmd.ExecuteNonQuery()

        WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Affected Rows: " & AffectedRows & vbCrLf)

        Cn.Close()

        Return AffectedRows
    End Function
    Private Function AppendXMP(ByVal PDFPath As String, ByVal XMPPath As String) As Boolean

        Dim Reader As New PdfReader(PDFPath)

        Dim xmpPDF As String = Replace(PDFPath, ".pdf", "_xmp.pdf")

        Dim xmpBite As Byte() = File.ReadAllBytes(XMPPath)
        Dim OldXmp As String = ""
        Dim OldDesc As String = ""

        If xmpBite IsNot Nothing Then
            OldXmp = New UTF8Encoding().GetString(xmpBite)
            OldXmp = Replace(OldXmp, vbCrLf, vbCr, 1, -1, CompareMethod.Text)
            OldDesc = Regex.Matches(OldXmp, "<dc:description>\s*<rdf:Alt>\s*<rdf:li [^>]+>(.*?)<\/rdf:li>", RegexOptions.IgnoreCase And RegexOptions.Multiline).Item(0).Groups(1).Value
        Else
            Reader.Close()
            Reader.Dispose()

            'FailureDesc = "Problem in reading XMP (" & XMPPath & ")"
            Return False
        End If

        'FileStream FileObj = new FileStream(xmpPDF, FileMode.OpenOrCreate, FileAccess.ReadWrite);

        Dim FileObj = New FileStream(xmpPDF, FileMode.Create)

        Dim Stamper As New PdfStamper(Reader, FileObj)

        Stamper.XmpMetadata = xmpBite

        Stamper.Close()
        Stamper.Dispose()
        Reader.Close()
        Reader.Dispose()
        FileObj.Close()
        FileObj.Dispose()

        'Reader = New PdfReader(xmpPDF)
        'Dim addedXmp As Byte() = Reader.Metadata

        If File.Exists(xmpPDF) Then
            Dim Reader1 As New PdfReader(xmpPDF)
            Dim b1 As Byte() = Reader1.Metadata
            Dim NewXmp As String = ""
            If b1 IsNot Nothing Then
                NewXmp = New UTF8Encoding().GetString(b1)
                Dim NewDesc As String = ""
                NewXmp = Replace(NewXmp, vbCrLf, vbCr, 1, -1, CompareMethod.Text)
                NewDesc = Regex.Matches(NewXmp, "<dc:description>\s*<rdf:Alt>\s*<rdf:li [^>]+>(.*?)<\/rdf:li>", RegexOptions.IgnoreCase And RegexOptions.Multiline And RegexOptions.Singleline).Item(0).Groups(1).Value
                If NewDesc <> "" And OldDesc <> NewDesc Then
                    Reader1.Close()
                    Reader1.Dispose()
                    'FailureDesc = "Problem in writing XMP. Difference found between XMP description and PDF description"
                    Return False
                End If
            Else
                Reader1.Close()
                Reader1.Dispose()

                'FailureDesc = "Problem in writing XMP (" & XMPPath & ")"
                Return False

            End If


            Reader1.Close()
            Reader1.Dispose()

            Try
                File.Delete(PDFPath)
                File.Copy(xmpPDF, PDFPath)
                File.Delete(xmpPDF)
            Catch ex As Exception
                'FailureDesc = ex.Message
                Return False
            End Try
        Else
            'FailureDesc = "XMP PDF not generated"
            Return False
        End If

        Return True
    End Function
    Public Function MailViaRelay(ByVal sFrom As String, ByVal sTo As String,
                           ByVal sCc As String, ByVal sBCC As String,
                            ByVal sSubject As String, ByVal sBody As String, ByVal Attachments As List(Of String)) As Boolean

        sBody = Replace(sBody, vbCrLf, "<br>", 1, -1, CompareMethod.Text)
        sBody = Replace(sBody, Chr(10), "<br>", 1, -1, CompareMethod.Text)

        Try
            Dim mSmtpClient As New SmtpClient()
            Dim mMailMessage As New System.Net.Mail.MailMessage()
            mMailMessage.From = New MailAddress(sFrom.Trim.Trim(";"))


            If sTo.Trim <> "" Then
                Dim strToSplit As String() = sTo.Trim.Split(";")
                For i As Integer = 0 To strToSplit.Length - 1
                    If strToSplit(i).ToString.Trim <> "" Then
                        mMailMessage.To.Add(strToSplit(i).ToString.Trim(";"))
                    End If
                Next
            End If

            If sCc.Trim <> "" Then
                Dim strCCSplit As String() = sCc.Trim.Split(";")
                For i As Integer = 0 To strCCSplit.Length - 1
                    If strCCSplit(i).ToString.Trim <> "" Then
                        mMailMessage.CC.Add(strCCSplit(i).Trim.Trim(";"))
                    End If
                Next
            End If

            If sBCC.Trim <> "" Then
                Dim strBCCSplit As String() = sBCC.Trim.Split(";")
                For i As Integer = 0 To strBCCSplit.Length - 1
                    If strBCCSplit(i).ToString.Trim <> "" Then
                        mMailMessage.Bcc.Add(strBCCSplit(i).Trim.Trim(";"))
                    End If
                Next

            End If

            'If strAttachment.Trim <> "" Then
            For Each strAttachment As String In Attachments
                Dim AttName As String = Regex.Match(strAttachment, "[^\\]+$").Value
                Dim TmpAttachment As String = Path.GetTempPath & AttName
                Try
                    File.Copy(strAttachment, TmpAttachment, True)
                Catch ex As Exception
                    MsgBox("Could not copy attachment into temp folder", MsgBoxStyle.Critical)
                End Try

                Try
                    Dim attachment As Net.Mail.Attachment = New System.Net.Mail.Attachment(TmpAttachment)
                    mMailMessage.Attachments.Add(attachment)

                Catch ex As Exception
                    'MsgBox(ex.Message)
                    'Exit Function
                    'If MsgBox("Could not attach the file. Please close the below file and click Retry!" & vbCrLf & strAttachment, MsgBoxStyle.RetryCancel) = MsgBoxResult.Retry Then
                    '    GoTo Retry
                    'Else
                    MsgBox("Attachment failed for " & strAttachment, MsgBoxStyle.Critical)
                    'End If
                End Try

                Try
                    File.Delete(TmpAttachment)
                Catch ex As Exception

                End Try

            Next
            'End If

            'mMailMessage.Bcc.Add("tiworkflow@novatechset.com")

            mMailMessage.Subject = sSubject
            mMailMessage.Body = sBody
            mMailMessage.IsBodyHtml = True

            mSmtpClient.Host = RelayServer()


            mSmtpClient.Send(mMailMessage)

            'sbTextBoxStatus("Mail Sent Successfully for the article " & strArticle.Trim & " --To-" & sTo & "--CC-" & sCc & "--BCC-" & sBCC, "blue", "NASPProofing")

            Return True
        Catch ex As System.Exception
            'sbTextBoxStatus("Mail Sent error....103.249.206.190 " & ex.ToString, "blue", "NASPProofing")
            Return False
        End Try
    End Function
    Public Function RelayServer()
        Return ReadFile("\\techsetserver1\software\General\RelayServer.txt").Trim
    End Function

    Sub preivewfilecheck(ByVal JTSArticleid As String, ByVal flagchk As Boolean)
        ', stageidcheck As String = ""
        WriteLog(Format(Now, "HH:mm:ss") & vbTab & JTSArticleid & " " & flagchk.ToString() & vbCrLf)
        'Exit Sub

        If JTSArticleid = "" Then
            Dim AllAttachments As New List(Of String)
            'If MailViaRelay("automailer@novatechset.com", "vijayakumar@novatechset.com", "", "", "SmartTrack Article ID identification problem: " & singleFile1.Name, "Hi," & vbCrLf & vbCrLf & singleFile1.Name, AllAttachments) = True Then
            'Else
            'MsgBox("Unable to send Mail!", MsgBoxStyle.Critical)
            'End If
            Me.Timer1.Stop()
            MsgBox("Could not get the article name.", MsgBoxStyle.Critical)
        End If

        Dim bookignjraid As String = ""
        Dim bookingjrstid As String = ""
        Dim bookingjrnltrnsid As String = ""
        Dim proofpagecount As String = ""
        Dim bookingstage As String = ""
        Dim jrstprocessid As String = ""
        Dim jrststage As String = ""
        Dim stageid As String = ""
        Dim bookprocessid As String = ""

        Dim sql1 As String = ""
        Try

            Dim con As New SqlClient.SqlConnection(DbConnectionString)
            Dim cmd As New SqlClient.SqlCommand
            Dim dr As SqlClient.SqlDataReader
            Dim bookignstage As String = ""

            Dim Cn As New SqlConnection(DbConnectionString)
            Cn.Open()

            sql1 = "SELECT JRA_ID as jraid,JRST_ID as jrstid,jrst_RevisedRecdt,JRST_ScheduledDt,JRST_UploadedDT,JRST_JRS_ID as jrsid,JrnlTrns_Id as jrnltrnsid ,JRST_Status as jrststatus,JrnlTrns_Status as jrnltrnsstatus,JrnlTrns_JType as jtype,JrnlTrns_NextProcessID as jrnlnextprocessid,Pause_ProcessCd as nextprocessid FROM JR_Articles with(nolock) ,JR_Stage_Tran with(nolock),TrnsJournalRev with(nolock) where JRA_JRST_ID=JRST_ID AND JRA_StageID in('93','94','95','96','107','108','136','137','138') and JRST_UploadedDT is null and JRA_StageID=JRST_JRS_ID and Jra_TransID=jrnltrns_id and JrnlTrns_Status='completed' and JRA_ArticleID in ('" & JTSArticleid.Trim & "') and JRST_Status='112'  "
            cmd = New SqlClient.SqlCommand(sql1, Cn)
            dr = cmd.ExecuteReader
            If dr.Read Then
                stageid = dr.Item("jrsid")
                If Trim("" & stageid) = "93" Then
                    bookignstage = "TandF-Preview"
                ElseIf Trim("" & stageid) = "94" Then
                    bookignstage = "TandF-Preview1"
                ElseIf Trim("" & stageid) = "95" Then
                    bookignstage = "TandF-Preview2"
                ElseIf Trim("" & stageid) = "96" Then
                    bookignstage = "TandF-Preview3"
                ElseIf Trim("" & stageid) = "107" Then
                    bookignstage = "TandF-Preview4"
                ElseIf Trim("" & stageid) = "108" Then
                    bookignstage = "TandF-Preview5"
                ElseIf Trim("" & stageid) = "136" Then
                    bookignstage = "TandF-Preview6"
                ElseIf Trim("" & stageid) = "137" Then
                    bookignstage = "TandF-Preview7"
                ElseIf Trim("" & stageid) = "138" Then
                    bookignstage = "TandF-Preview8"
                Else
                    WriteLog(Format(Now, "HH:mm:ss") & vbTab & "Unable to get Booking Stage" & vbCrLf)
                    MsgBox("Unable to get Booking Stage")
                End If
                bookignjraid = dr.Item("jraid")
                bookingjrstid = dr.Item("jrstid")
                bookingjrnltrnsid = dr.Item("jrnltrnsid")
                bookprocessid = dr.Item("nextprocessid")
                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "jraid" & bookignjraid & vbCrLf)
                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "jrstid" & bookingjrstid & vbCrLf)
                WriteLog(Format(Now, "HH:mm:ss") & vbTab & "nextprocessid" & bookprocessid & vbCrLf)

            End If
            dr.Close()
            Cn.Close()
            If bookingjrstid <> "" AndAlso bookignjraid <> "" Then

                ' SUCESSS()
                If flagchk = True Then
                    Dim AffRows As Integer
                    AffRows = UpdateSql("UPDATE JR_Articles SET JRA_Status='129' where JRA_ID in ('" & bookignjraid & "')")
                    AffRows = 0
                    AffRows = UpdateSql("UPDATE JR_Stage_Tran SET JRST_Status='129' where JRST_ID in ('" & bookingjrstid & "')")
                    AffRows = 0
                    AffRows = UpdateSql("insert into TrnsJournalRev(JrnlTrns_ArticleId,JrnlTrns_Emp,JrnlTrns_Dept,JrnlTrns_InTime,JrnlTrns_OutTime,JrnlTrns_ProcessCd,JrnlTrns_Status,JrnlTrns_NextProcessID,JrnlTrns_Remarks,LastModifiedDate,UsrID,JrnlTrns_JType) values ('" & bookignjraid & "' ,'1','Admin',getdate(),getdate(),'112','Completed','129','Auto Entry moved from pearl pdf enable',getdate(),'1','" & bookignstage & "')")
                    '307-PDF-ERR
                    '129-Preview
                End If

                If flagchk = False Then
                    Dim AffRows As Integer
                    AffRows = UpdateSql("UPDATE JR_Articles SET JRA_Status='307' where JRA_ID in ('" & bookignjraid & "')")
                    AffRows = 0
                    AffRows = UpdateSql("UPDATE JR_Stage_Tran SET JRST_Status='307' where JRST_ID in ('" & bookingjrstid & "')")
                    AffRows = 0
                    AffRows = UpdateSql("insert into TrnsJournalRev(JrnlTrns_ArticleId,JrnlTrns_Emp,JrnlTrns_Dept,JrnlTrns_InTime,JrnlTrns_OutTime,JrnlTrns_ProcessCd,JrnlTrns_Status,JrnlTrns_NextProcessID,JrnlTrns_Remarks,LastModifiedDate,UsrID,JrnlTrns_JType) values ('" & bookignjraid & "' ,'1','Admin',getdate(),getdate(),'112','Completed','307','Auto Entry moved from pearl pdf enable',getdate(),'1','" & bookignstage & "')")
                End If

            End If

        Catch ex As Exception
            WriteLog(Format(Now, "HH:mm:ss") & vbTab & ex.Message & vbCrLf)
            'SaveTextToFile("Database update Error " & gstrSQL, 1)

        End Try
    End Sub

End Class
